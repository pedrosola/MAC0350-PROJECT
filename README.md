
# Projeto DBZ

Devido a falta de experiencia com a linguagem vue.js e tempo escasso, apenas parte do ep foi implementado, com foco maior nos aspectos visuais do sistema.

NOTE: O arquivo server/docker-compose foi alterado para que funcionasse neste computador. Para alterar de volta ao padrão, adicione ':5432' à linha 7 do arquivo.

Ainda que feito individualmente, tentei explorar os componentes de html e css que já conhecia com alguns exemplos presentes na internet.

Author: Pedro Sola Pimentel - NUSP: 9298079

O repositorio deste projeto esta disponivel em:
https://gitlab.com/pedrosola/MAC0350-PROJECT

Project developed during the course MAC0350
(Introduction to Systems Development) at IME-USP.

This repository is a monorepo and requires [docker][1] and
[docker-compose][2] to run the services.

In order to setup the back-end services, open a shell and run:
```bash
cd server
docker-compose up
```

In order to setup the front-end services, open another shell and run:
```bash
cd client
docker-compose up
```

[1]: https://store.docker.com/search?type=edition&offering=community
[2]: https://docs.docker.com/compose/install/
